import { Component, OnInit } from '@angular/core';
import { Observable, take } from 'rxjs';
import { selectListItems } from 'src/app/state/selectors/items.selectors';
import { AppState } from 'src/app/state/app.state';
import { Store } from '@ngrx/store';
import { Imagen } from '../../models/imagen';

@Component({
  selector: 'sm-list-images',
  templateUrl: './list-images.component.html',
  styleUrls: ['./list-images.component.scss'],
})
export class ListImagesComponent  implements OnInit {

  items$: Observable<any> = new Observable();
  myPageList: any[] = [];
  
  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.items$ = this.store.select(selectListItems);
  }

}
