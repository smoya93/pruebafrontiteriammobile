import { Component, Input, OnInit } from '@angular/core';
import { Imagen } from '../../models/imagen';

@Component({
  selector: 'sm-item-images',
  templateUrl: './item-images.component.html',
  styleUrls: ['./item-images.component.scss'],
})
export class ItemImagesComponent  implements OnInit {

  @Input() itemImage!: Imagen;

  constructor() {
   }

  ngOnInit() {}

}
