import { ComponentFixture, fakeAsync, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { Imagen } from '../../models/imagen';

import { ItemImagesComponent } from './item-images.component';

describe('ItemImagesComponent', () => {
  let component: ItemImagesComponent;
  let fixture: ComponentFixture<ItemImagesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ItemImagesComponent],
      imports: [IonicModule.forRoot()],
      teardown: { destroyAfterEach: false }
    }).compileComponents();

    fixture = TestBed.createComponent(ItemImagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const itemImage: Imagen =
    {
      id: "12",
      photo: "https://picsum.photos/300?random=12",
      text: "simply dummy text of the printing and typesetting industry. Take a type specimen book."
    };
    component.itemImage = itemImage;
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });  

  it('should show an image detail', () => {
    component.itemImage = {
      id: "12",
      photo: "https://picsum.photos/300?random=12",
      text: "simply dummy text of the printing and typesetting industry. Take a type specimen book."
    };
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('ion-card-title').innerText).toEqual('12');
  });


});
