export interface Imagen {
    id: string;
    photo: string;
    text: string;
}
