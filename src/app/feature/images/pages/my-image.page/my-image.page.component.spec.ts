import { ComponentFixture, inject, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MyImagePageComponent } from './my-image.page.component';
import { Imagen } from '../../models/imagen';
import { of } from 'rxjs';
import { DataImagenService } from '../../services/data-imagen.service';

describe('MyImagePageComponent', () => {
  let component: MyImagePageComponent;
  let fixture: ComponentFixture<MyImagePageComponent>;
  let dataImagenService: any;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [MyImagePageComponent],
      imports: [IonicModule.forRoot()],
      providers: [
        DataImagenService
    ]
    }).compileComponents();

    fixture = TestBed.createComponent(MyImagePageComponent);
    component = fixture.componentInstance;
    dataImagenService = TestBed.get(DataImagenService);
    fixture.detectChanges();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(MyImagePageComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  });

  it("should call getDataImageList and return list of images", (() => {
    const response: Imagen[] = [];
    spyOn(dataImagenService, 'getDataListImagen').and.returnValue(of(response))
    component.getDataImageList();
    fixture.detectChanges();
    expect(component.myPageList).toEqual(response);
  }));

});
