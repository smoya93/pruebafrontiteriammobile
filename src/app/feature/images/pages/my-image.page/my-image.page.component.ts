import { Component, OnInit } from '@angular/core';
import { Observable, Subscription, take } from 'rxjs';
import { ProgressbarService } from 'src/app/shared/services/progressbar.service';
import { Imagen } from '../../models/imagen';
import { DataImagenService } from '../../services/data-imagen.service';
import { Store } from '@ngrx/store';
import { selectLoading } from 'src/app/state/selectors/items.selectors';
import { filterItems, loadItems } from 'src/app/state/actions/items.actions';
import { loremIpsum } from 'lorem-ipsum';

@Component({
  selector: 'app-my-image.page',
  templateUrl: './my-image.page.component.html',
  styleUrls: ['./my-image.page.component.scss'],
})
export class MyImagePageComponent implements OnInit {

  myPageList: any[] = [];
  myPageListFilter: Imagen[] = [];
  filterTerm!: string;
  items: any[] = [];
  loading$: Observable<boolean> = new Observable();

  constructor(private progressBarService: ProgressbarService, private store: Store<any>) {
  }

  async ngOnInit() {
    /* await this.progressBarService.show(); */
    /* this.getDataImageList(); */
    this.loading$ = this.store.select(selectLoading);//TODO: true, false
    this.store.dispatch(loadItems()) //TODO🔴
  }

  /* getDataImageList() {  
    this.dataImageService.getDataListImagen().pipe(take(1)).subscribe({
      next: (response: Imagen[]) => {
        this.myPageList = response;
        this.myPageListFilter = this.myPageList;
        this.progressBarService.hide();
      },
      error: (error) => {
        this.progressBarService.hide();
        console.error(error);
      },
      complete: () => { }
    });
  } */

  search(query: Event) {
    this.myPageListFilter = (!query) ? [...this.myPageList] : this.myPageListFilter = this.myPageList.filter((image) => {
      return (image.id.includes((query.target as HTMLInputElement).value) || image.text.includes((query.target as HTMLInputElement).value));
    })

    this.myPageListFilter = [
      {
        id: "Foto Imagen 1",
        photo: "https://picsum.photos/300?random=1",
        text: loremIpsum()
      }
    ]

    console.log(this.myPageListFilter);
    this.store.dispatch(filterItems({items: this.myPageListFilter}));
  }
}
