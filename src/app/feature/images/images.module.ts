import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ImagesRoutingModule } from './images-routing.module';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MyImagePageComponent } from './pages/my-image.page/my-image.page.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';  
import { ItemImagesComponent } from './components/item-images/item-images.component';
import { ListImagesComponent } from './components/list-images/list-images.component';


@NgModule({
  declarations: [MyImagePageComponent, ItemImagesComponent, ListImagesComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ImagesRoutingModule,
    Ng2SearchPipeModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
})
export class ImagesModule { }

