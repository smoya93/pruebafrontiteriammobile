import { Injectable } from '@angular/core';
import { loremIpsum } from 'lorem-ipsum';
import { Observable, of, delay, tap } from 'rxjs';
import { Imagen } from '../models/imagen';

@Injectable({
  providedIn: 'root'
})
export class DataImagenService {

  limitRamdom = 3;
  delaySimulation = 0;

  constructor() { }

  public getDataListImagen(): Observable<Imagen[]> {
    return of(this.getDataRamdom(this.limitRamdom)).pipe(delay(this.delaySimulation), tap(results => {
      results.sort(function(a, b) {
        return parseInt(a.id) - parseInt(b.id);
      })
    }));
  }

  public getDataRamdom(limitRandom: number): Imagen[] {
    const newItems: Imagen[] = [];
    for (let i = 0; i < limitRandom; i++) {
      const id = Math.round(Math.floor(Math.random() * 6) + (i *6));
      const photo = `https://picsum.photos/300?random=${id}`;
      const text = loremIpsum();
      newItems.push({
        id: id.toString(),
        photo: photo,
        text: text
      });
    }
    return newItems;
  }
}
