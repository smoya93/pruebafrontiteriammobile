import { inject, TestBed } from '@angular/core/testing';
import { DataImagenService } from './data-imagen.service';

describe('DataImagenService', () => {
  let service: DataImagenService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      teardown: {destroyAfterEach: false}   // <- add this line
    });
    service = TestBed.inject(DataImagenService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('retrieves all the list', inject([DataImagenService], (DataImagenService: any) => {
    DataImagenService.getDataListImagen(4).subscribe((result: any) => expect(result.length).toBeGreaterThan(0));
  })
  );

  it("should create a list of images in an array", () => {
    expect(service.getDataRamdom(4).length).toEqual(4);
  });

});
