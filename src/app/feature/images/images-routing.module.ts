import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyImagePageComponent } from './pages/my-image.page/my-image.page.component';

const routes: Routes = [
  {
    path: '',
    component: MyImagePageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ImagesRoutingModule { }
