import { Imagen } from "src/app/feature/images/models/imagen";

export interface ItemsState {
    loading: boolean,
    items: ReadonlyArray<Imagen>;
}