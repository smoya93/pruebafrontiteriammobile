import { TestBed } from '@angular/core/testing';
import { AngularDelegate, PopoverController } from '@ionic/angular';

import { ProgressbarService } from './progressbar.service';

describe('ProgressbarService', () => {
  let service: ProgressbarService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PopoverController, AngularDelegate]
    });
    service = TestBed.inject(ProgressbarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
