import { Injectable } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { ProgressbarComponent } from '../components/progressbar/progressbar.component';

@Injectable({
  providedIn: 'root'
})
export class ProgressbarService {

  popover!: HTMLIonPopoverElement;

  constructor(private popoverController: PopoverController) {
  }

  async show()
  {
     this.popover = await this.popoverController.create({
           component: ProgressbarComponent,
           backdropDismiss: false
     });
      this.popover.present();
  }

  async hide()
  {
      this.popover.dismiss();
  }
}
