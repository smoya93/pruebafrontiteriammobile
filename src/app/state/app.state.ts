import { ActionReducerMap } from "@ngrx/store";
import { itemsReducer } from "./reducers/items.reducers";
import { ItemsState } from "../core/models/Item.state";

export interface AppState {
    items: ItemsState;
}

export const ROOT_REDUCERS: ActionReducerMap<AppState> = {
    items: itemsReducer
}