import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects'; //TODO <---
import { EMPTY } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { DataImagenService } from 'src/app/feature/images/services/data-imagen.service';

@Injectable()
export class ItemsEffects {

    loadItems$ = createEffect(() => this.actions$.pipe(
        ofType('[Item List] Load items'),
        mergeMap(() => this.dataImagenService.getDataListImagen()//TODO Retorna la data [...]
            .pipe(
                map(items => ({ type: '[Item List] Loaded success', items })),
                catchError(() => EMPTY)
            ))
    )
    );

    constructor(
        private actions$: Actions,
        private dataImagenService: DataImagenService
    ) { }
}