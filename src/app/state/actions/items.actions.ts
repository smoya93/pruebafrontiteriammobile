import { createAction, props } from '@ngrx/store'; //TODO: <----
import { Imagen } from 'src/app/feature/images/models/imagen';


export const loadItems = createAction(
    '[Item List] Load items' //TODO type*
);

export const loadedItems = createAction(
    '[Item List] Loaded success',
    props<{ items: Imagen[] }>()
)

export const filterItems = createAction(
    '[Item List] Filter items',
    props<{ items: Imagen[] }>()
)